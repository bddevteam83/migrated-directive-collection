import {Directive, ElementRef, OnInit} from '@angular/core';

@Directive({
  selector: '[bdNextInputFocus]'
})
export class NextInputFocusDirective implements OnInit {

  container;

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit(): void {
    this.container = this.elementRef.nativeElement;

    this.container.onkeyup = function (e) {
      const target = e.srcElement;
      const maxLength = parseInt(target.attributes['maxlength'].value, 10);
      const myLength = target.value.length;
      if (myLength >= maxLength) {
        let next = target;
        while (next = next.nextElementSibling) {
          if (next == null) {
            break;
          }
          if (next.tagName.toLowerCase() === 'input') {
            next.focus();
            break;
          }
        }
      }
    };
  }

}
