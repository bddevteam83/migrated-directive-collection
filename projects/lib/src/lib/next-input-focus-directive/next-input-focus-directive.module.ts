import {NgModule} from '@angular/core';
import {NextInputFocusDirective} from './next-input-focus.directive';

@NgModule({
  declarations: [NextInputFocusDirective],
  exports: [NextInputFocusDirective]
})
export class NextInputFocusDirectiveModule {
}
