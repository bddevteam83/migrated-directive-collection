import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[bdLongPress]'
})
export class LongPressDirective {

  @Output() bdLongPress: EventEmitter<void> = new EventEmitter();

  private _timeout: any;

  @HostListener('mousedown') onMouseDown() {
    this._timeout = setTimeout(() => {
      this.bdLongPress.emit();
    }, 500);
  }

  @HostListener('mouseup') onMouseUp() {
    clearTimeout(this._timeout);
  }

  @HostListener('mouseleave') onMouseLeave() {
    clearTimeout(this._timeout);
  }

}
