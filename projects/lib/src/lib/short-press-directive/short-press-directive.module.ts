import {NgModule} from '@angular/core';
import {ShortPressDirective} from './short-press.directive';

@NgModule({
  declarations: [ShortPressDirective],
  exports: [ShortPressDirective]
})
export class ShortPressDirectiveModule {
}
