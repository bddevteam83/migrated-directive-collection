import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[bdShortPress]'
})
export class ShortPressDirective {

  @Output() bdShortPress: EventEmitter<void> = new EventEmitter();

  private _timeout: any;
  private _isShort: boolean;

  @HostListener('mousedown') onMouseDown() {
    this._isShort = true;
    this._timeout = setTimeout(() => {
      this._isShort = false;
    }, 100);
  }

  @HostListener('mouseup') onMouseUp() {
    if (this._isShort) {
      this.bdShortPress.emit();
    }
    clearTimeout(this._timeout);
  }

  @HostListener('mouseleave') onMouseLeave() {
    clearTimeout(this._timeout);
  }

}
