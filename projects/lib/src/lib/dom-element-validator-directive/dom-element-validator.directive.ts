import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';

import * as jwt_decode_imported from 'jwt-decode';
import * as _ from 'lodash';

const jwt_decode = jwt_decode_imported;

@Directive({
  selector: '[bdDomElementValidator]'
})
export class DomElementValidatorDirective {

  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef,
              private $SessionStorageService: SessionStorageService
  ) {
  }

  @Input('bdDomElementValidator') set elementRole(elementRole: string) {
    if (!elementRole || this.isAuthorized(elementRole)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  private isAuthorized(auth): boolean {
    const token = this.$SessionStorageService.retrieve('authenticationToken');
    if (token !== null && token !== undefined) {
      const jwtToken = jwt_decode(token);
      return _.includes((<any>jwtToken).AUTH, auth);
    } else {
      console.error('Token is empty');
      return false;
    }
  }
}
