import {NgModule} from '@angular/core';
import {DomElementValidatorDirective} from './dom-element-validator.directive';

@NgModule({
  declarations: [DomElementValidatorDirective],
  exports: [DomElementValidatorDirective]
})
export class DomElementValidatorDirectiveModule {
}
